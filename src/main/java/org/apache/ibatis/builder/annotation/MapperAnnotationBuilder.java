/**
 * Copyright 2009-2020 the original author or authors.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.ibatis.builder.annotation;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.CacheNamespaceRef;
import org.apache.ibatis.annotations.Case;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Lang;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Options.FlushCachePolicy;
import org.apache.ibatis.annotations.Property;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.TypeDiscriminator;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.binding.MapperMethod.ParamMap;
import org.apache.ibatis.builder.BuilderException;
import org.apache.ibatis.builder.CacheRefResolver;
import org.apache.ibatis.builder.IncompleteElementException;
import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.executor.keygen.Jdbc3KeyGenerator;
import org.apache.ibatis.executor.keygen.KeyGenerator;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.executor.keygen.SelectKeyGenerator;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.Discriminator;
import org.apache.ibatis.mapping.FetchType;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultFlag;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.mapping.ResultSetType;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.parsing.PropertyParser;
import org.apache.ibatis.reflection.TypeParameterResolver;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.apache.ibatis.type.UnknownTypeHandler;

/**
 * @author Clinton Begin
 * @author Kazuki Shimizu
 */
public class MapperAnnotationBuilder {

  private static final Set<Class<? extends Annotation>> statementAnnotationTypes = Stream
    .of(Select.class, Update.class, Insert.class, Delete.class, SelectProvider.class, UpdateProvider.class,
      InsertProvider.class, DeleteProvider.class)
    .collect(Collectors.toSet());
  //全局配置类
  private final Configuration configuration;
  //mapper解析助手
  private final MapperBuilderAssistant assistant;
  //mapper类型
  private final Class<?> type;

  public MapperAnnotationBuilder(Configuration configuration, Class<?> type) {
    //通过全限定名推出 该类的资源地址
    String resource = type.getName().replace('.', '/') + ".java (best guess)";
    //mapper建造助手类
    this.assistant = new MapperBuilderAssistant(configuration, resource);
    //配置类
    this.configuration = configuration;
    //类型
    this.type = type;
  }

  public void parse() {
    String resource = type.toString();
    if (!configuration.isResourceLoaded(resource)) {      //配置类中该资源未被加载进行加载
      //加载mapper相对于的xml 以及解析xml中mapper的内容

      loadXmlResource();
      //配置类 中添加resource的记录 便于判断是否改资源已经加载
      configuration.addLoadedResource(resource);
      //将类名作为命名空间
      assistant.setCurrentNamespace(type.getName());
      //解析是否要缓存命名空间 与类一对一
      parseCache();
      //解析是否要缓存命名空间引用 与类 一对多
      parseCacheRef();
      //代理mapper方法
      for (Method method : type.getMethods()) {
        if (!canHaveStatement(method)) {
          //是桥接或者default方法不进行代理
          continue;
        }
        //@Select和@SelectProvider的方法进行解析
        //并且改方法上不存在@ResultMap注解
        if (getAnnotationWrapper(method, false, Select.class, SelectProvider.class).isPresent()
          && method.getAnnotation(ResultMap.class) == null) {
          //缺失@ResultMap情况下的 解析
          parseResultMap(method);
        }
        try {
          //解析动态sql Statement JDBC的Statement，PreStatement都是Statement(此次不止解析Statement还解析了方法上的 @Options存储等系列方法上的注解)
          parseStatement(method);
        } catch (IncompleteElementException e) {
          //解析出行异常 下文parsePendingMethods()会对解析出行异常的再次解析
          configuration.addIncompleteMethod(new MethodResolver(this, method));
        }
      }
    }
    //解析失败的方法重新 解析
    parsePendingMethods();
  }

  private boolean canHaveStatement(Method method) {
    // issue #23
    //判断是不是否桥接 或者 default 方法
    return !method.isBridge() && !method.isDefault();
  }

  private void parsePendingMethods() {
    Collection<MethodResolver> incompleteMethods = configuration.getIncompleteMethods();
    synchronized (incompleteMethods) {
      Iterator<MethodResolver> iter = incompleteMethods.iterator();
      while (iter.hasNext()) {
        try {
          iter.next().resolve();
          iter.remove();
        } catch (IncompleteElementException e) {
          // This method is still missing a resource
        }
      }
    }
  }

  private void loadXmlResource() {
    // Spring may not know the real resource name so we check a flag Spring可能不知道真实的资源名称，因此我们检查一个标志
    // to prevent loading again a resource twice 防止再次加载资源两次
    // this flag is set at XMLMapperBuilder#bindMapperForNamespace 此标志在XMLMapperBuilder＃bindMapperForNamespace设置
    if (!configuration.isResourceLoaded("namespace:" + type.getName())) {
      //type对应的xml资源路径 是否存在相对于的xml
      String xmlResource = type.getName().replace('.', '/') + ".xml";
      // #1347
      InputStream inputStream = type.getResourceAsStream("/" + xmlResource);
      if (inputStream == null) {
        // Search XML mapper that is not in the module but in the classpath. 搜索不在模块中但在类路径中的XML mapper。
        try {
          //类路径中加载xml
          inputStream = Resources.getResourceAsStream(type.getClassLoader(), xmlResource);
        } catch (IOException e2) {
          // ignore, resource is not required
        }
      }
      if (inputStream != null) {
        XMLMapperBuilder xmlParser = new XMLMapperBuilder(inputStream, assistant.getConfiguration(), xmlResource, configuration.getSqlFragments(), type.getName());
        //xml 解析
        xmlParser.parse();
      }
    }
  }

  private void parseCache() {
    //判断mapper类是否存在@CacheNamespace注解
    CacheNamespace cacheDomain = type.getAnnotation(CacheNamespace.class);
    if (cacheDomain != null) {
      //缓存的大小
      Integer size = cacheDomain.size() == 0 ? null : cacheDomain.size();
      //缓存刷新的频率
      Long flushInterval = cacheDomain.flushInterval() == 0 ? null : cacheDomain.flushInterval();
      //缓存的Properties配置
      Properties props = convertToProperties(cacheDomain.properties());
      //生成新的缓存
      assistant.useNewCache(cacheDomain.implementation(), cacheDomain.eviction(), flushInterval, size, cacheDomain.readWrite(), cacheDomain.blocking(), props);
    }
  }

  private Properties convertToProperties(Property[] properties) {
    if (properties.length == 0) {
      return null;
    }
    Properties props = new Properties();
    for (Property property : properties) {
      props.setProperty(property.name(),
        PropertyParser.parse(property.value(), configuration.getVariables()));
    }
    return props;
  }

  private void parseCacheRef() {
    //从mapper类获取@CacheNamespaceRef注解信息
    CacheNamespaceRef cacheDomainRef = type.getAnnotation(CacheNamespaceRef.class);
    if (cacheDomainRef != null) {
      //@CacheNamespaceRef 的refType和refName
      Class<?> refType = cacheDomainRef.value();
      String refName = cacheDomainRef.name();
      if (refType == void.class && refName.isEmpty()) {
        throw new BuilderException("Should be specified either value() or name() attribute in the @CacheNamespaceRef");
      }
      if (refType != void.class && !refName.isEmpty()) {
        throw new BuilderException("Cannot use both value() and name() attribute in the @CacheNamespaceRef");
      }
      //namespace更具refType和refName二选一 优先refType
      String namespace = (refType != void.class) ? refType.getName() : refName;
      try {
        assistant.useCacheRef(namespace);
      } catch (IncompleteElementException e) {
        configuration.addIncompleteCacheRef(new CacheRefResolver(assistant, namespace));
      }
    }
  }

  private String parseResultMap(Method method) {
    //获取方法的放回类型
    Class<?> returnType = getReturnType(method);
    //获取方法上的@Arg注解数组 参见@ConstructorArgs注解
    Arg[] args = method.getAnnotationsByType(Arg.class);
    //获取方法上的@Result注解数组 参见@Results
    Result[] results = method.getAnnotationsByType(Result.class);
    //获取方法上的@TypeDiscriminator注解 参见@TypeDiscriminator
    TypeDiscriminator typeDiscriminator = method.getAnnotation(TypeDiscriminator.class);
    //生成resultMap的唯一标识
    String resultMapId = generateResultMapName(method);
    //生成ResultMap
    applyResultMap(resultMapId, returnType, args, results, typeDiscriminator);
    return resultMapId;
  }

  private String generateResultMapName(Method method) {
    Results results = method.getAnnotation(Results.class);
    if (results != null && !results.id().isEmpty()) {
      return type.getName() + "." + results.id();
    }
    StringBuilder suffix = new StringBuilder();
    for (Class<?> c : method.getParameterTypes()) {
      suffix.append("-");
      suffix.append(c.getSimpleName());
    }
    if (suffix.length() < 1) {
      suffix.append("-void");
    }
    return type.getName() + "." + method.getName() + suffix;
  }

  private void applyResultMap(String resultMapId, Class<?> returnType, Arg[] args, Result[] results, TypeDiscriminator discriminator) {
    //存放ResultMapping的集合  ResultMapping是数据库与Java类字段对应关系的一个类 参见 ResultMapping
    List<ResultMapping> resultMappings = new ArrayList<>();
    //解析@ConstructorArgs中@Arg的配置生成ResultMapping列表数据 二选一
    applyConstructorArgs(args, returnType, resultMappings);
    //解析@Results中的@Result中的配置生成ResultMapping列表数据 二选一
    applyResults(results, returnType, resultMappings);
    //解析@TypeDiscriminator中的@Case的配置 参见@TypeDiscriminator和@Case
    Discriminator disc = applyDiscriminator(resultMapId, returnType, discriminator);
    // TODO add AutoMappingBehaviour
    //生成改方法@ConstructorArgs或者@Results指定的ResultMap并且添加到配置类中记录
    assistant.addResultMap(resultMapId, returnType, null, disc, resultMappings, null);
    //@case中仍然有 @ConstructorArgs或者@Results继续解析生成ResultMap
    createDiscriminatorResultMaps(resultMapId, returnType, discriminator);
  }

  private void createDiscriminatorResultMaps(String resultMapId, Class<?> resultType, TypeDiscriminator discriminator) {
    if (discriminator != null) {
      for (Case c : discriminator.cases()) {
        String caseResultMapId = resultMapId + "-" + c.value();
        List<ResultMapping> resultMappings = new ArrayList<>();
        // issue #136
        applyConstructorArgs(c.constructArgs(), resultType, resultMappings);
        applyResults(c.results(), resultType, resultMappings);
        // TODO add AutoMappingBehaviour
        assistant.addResultMap(caseResultMapId, c.type(), resultMapId, null, resultMappings, null);
      }
    }
  }

  private Discriminator applyDiscriminator(String resultMapId, Class<?> resultType, TypeDiscriminator discriminator) {
    if (discriminator != null) {
      String column = discriminator.column();
      Class<?> javaType = discriminator.javaType() == void.class ? String.class : discriminator.javaType();
      JdbcType jdbcType = discriminator.jdbcType() == JdbcType.UNDEFINED ? null : discriminator.jdbcType();
      @SuppressWarnings("unchecked")
      Class<? extends TypeHandler<?>> typeHandler = (Class<? extends TypeHandler<?>>)
        (discriminator.typeHandler() == UnknownTypeHandler.class ? null : discriminator.typeHandler());
      Case[] cases = discriminator.cases();
      Map<String, String> discriminatorMap = new HashMap<>();
      for (Case c : cases) {
        String value = c.value();
        String caseResultMapId = resultMapId + "-" + value;
        discriminatorMap.put(value, caseResultMapId);
      }
      return assistant.buildDiscriminator(resultType, column, javaType, jdbcType, typeHandler, discriminatorMap);
    }
    return null;
  }

  void parseStatement(Method method) {
    //判断方法的参数类型
    final Class<?> parameterTypeClass = getParameterType(method);
    //LanguageDriver类型 @LanguageDriver可以根据来自定义 解析mybatis语句的驱动类
    final LanguageDriver languageDriver = getLanguageDriver(method);
    //更具方法上的注释 生成注释包装类 包装类具有databaseId(数据库id),sqlCommandType(改注释对应sql的操作类型),annotation(方法上的注释)
    getAnnotationWrapper(method, true, statementAnnotationTypes).ifPresent(statementAnnotation -> {
      //更具不同注解类型获取不同的sql数据源
      final SqlSource sqlSource = buildSqlSource(statementAnnotation.getAnnotation(), parameterTypeClass, languageDriver, method);
      //sql的类型
      final SqlCommandType sqlCommandType = statementAnnotation.getSqlCommandType();
      //获取方法上的@Options注解
      final Options options = getAnnotationWrapper(method, false, Options.class).map(x -> (Options) x.getAnnotation()).orElse(null);
      //mappedStatementId
      final String mappedStatementId = type.getName() + "." + method.getName();
      //key通用器 主要是对插入操作 插入之前获取key，插入之后获取key
      final KeyGenerator keyGenerator;
      //key 对应的字段
      String keyProperty = null;
      //key 对应的列
      String keyColumn = null;
      //插入或者更新操作
      if (SqlCommandType.INSERT.equals(sqlCommandType) || SqlCommandType.UPDATE.equals(sqlCommandType)) {
        // first check for SelectKey annotation - that overrides everything else
        //获取@SelectKey注释 主要应用场景获取id
        SelectKey selectKey = getAnnotationWrapper(method, false, SelectKey.class).map(x -> (SelectKey) x.getAnnotation()).orElse(null);
        if (selectKey != null) {
          //selectKey存在与方法上
          //更具@SelectKey执行生成KeyGenerator
          keyGenerator = handleSelectKeyAnnotation(selectKey, mappedStatementId, getParameterType(method), languageDriver);
          //由@SelectKey设置得知 key 对应的字段是那个
          keyProperty = selectKey.keyProperty();
        } else if (options == null) {
          //selectKey不存在与方法上切@options注解也不存在
          //设置keyGenerator默认为NoKeyGenerator不进行插入操作id拦截操作赋值，即默认id需要生成
          keyGenerator = configuration.isUseGeneratedKeys() ? Jdbc3KeyGenerator.INSTANCE : NoKeyGenerator.INSTANCE;
        } else {
          //selectKey不存在与方法上切@options注解存在
          //@options上是否要使用useGeneratedKeys 使用的话就是Jdbc3KeyGenerator对插入操作 id 拦截 id为自增
          keyGenerator = options.useGeneratedKeys() ? Jdbc3KeyGenerator.INSTANCE : NoKeyGenerator.INSTANCE;
          //key 对应的字段 从@options中配置获取
          keyProperty = options.keyProperty();
          //key 对应的列 从@options中配置获取
          keyColumn = options.keyColumn();
        }
      } else {
        //查询和删操作 设置NoKeyGenerator即默认id需要生成
        //查询和删操作 其实不需要id，当然也不需要keyGenerator去拦截id 所以为NoKeyGenerator不拦截
        keyGenerator = NoKeyGenerator.INSTANCE;
      }
      //拉取的大小 (结果集通过tcp传输，一次传输fetchSize条数据)
      Integer fetchSize = null;
      //超时时间
      Integer timeout = null;
      //预处理类型 # 默认是预处理类型    有三种类型STATEMENT, PREPARED, CALLABLE
      StatementType statementType = StatementType.PREPARED;
      //ResultSetType获取默认的cursor 类型
      /**
       * 1 ：ResultSet.TYPE_FORWARD_ONLY
       *
       * 默认的cursor 类型，仅仅支持结果集forward ，不支持backforward ，random ，last ，first 等操作。
       *
       * 2 ：ResultSet.TYPE_SCROLL_INSENSITIVE
       *
       * 支持结果集backforward ，random ，last ，first 等操作，对其它session 对数据库中数据做出的更改是不敏感的。
       *
       * 实现方法：从数据库取出数据后，会把全部数据缓存到cache 中，对结果集的后续操作，是操作的cache 中的数据，数据库中记录发生变化后，不影响cache 中的数据，所以ResultSet 对结果集中的数据是INSENSITIVE 的。
       *
       * 3 ：ResultSet.TYPE_SCROLL_SENSITIVE
       *
       * 支持结果集backforward ，random ，last ，first 等操作，对其它session 对数据库中数据做出的更改是敏感的，即其他session 修改了数据库中的数据，会反应到本结果集中。
       */
      ResultSetType resultSetType = configuration.getDefaultResultSetType();
      //是否select sql标识符
      boolean isSelect = sqlCommandType == SqlCommandType.SELECT;
      //非select sql需要刷新缓存
      boolean flushCache = !isSelect;
      //select sql需要使用缓存
      boolean useCache = isSelect;
      if (options != null) {
        //根据@options来判断本次sql是否要刷新缓存
        if (FlushCachePolicy.TRUE.equals(options.flushCache())) {
          flushCache = true;
        } else if (FlushCachePolicy.FALSE.equals(options.flushCache())) {
          flushCache = false;
        }
        //更具@options来确定是否要使用缓存
        useCache = options.useCache();
        //更具@options配置fetchSize
        fetchSize = options.fetchSize() > -1 || options.fetchSize() == Integer.MIN_VALUE ? options.fetchSize() : null; //issue #348
        //更具@options配置fetchSize
        timeout = options.timeout() > -1 ? options.timeout() : null;
        //更具@options配置statementType
        statementType = options.statementType();
        //更具@options配置resultSetType
        if (options.resultSetType() != ResultSetType.DEFAULT) {
          resultSetType = options.resultSetType();
        }
      }

      String resultMapId = null;
      if (isSelect) {
        //是select sql 解析方法上的@ResultMap
        ResultMap resultMapAnnotation = method.getAnnotation(ResultMap.class);
        if (resultMapAnnotation != null) {
          //resultMapId 设置为resultMapAnnotation指定的ResultMap
          resultMapId = String.join(",", resultMapAnnotation.value());
        } else {
          //无@ResultMap 使用之前@Results或者@@ConstructorArgs生成的缺省情况下的ResultMap
          resultMapId = generateResultMapName(method);
        }
      }
      //添加 MappedStatement (相当于一条sql 相关的所有信息)
      assistant.addMappedStatement(
        mappedStatementId,
        sqlSource,
        statementType,
        sqlCommandType,
        fetchSize,
        timeout,
        // ParameterMapID
        null,
        parameterTypeClass,
        resultMapId,
        getReturnType(method),
        resultSetType,
        flushCache,
        useCache,
        // TODO gcode issue #577
        false,
        keyGenerator,
        keyProperty,
        keyColumn,
        statementAnnotation.getDatabaseId(),
        languageDriver,
        // ResultSets
        options != null ? nullOrEmpty(options.resultSets()) : null);
    });
  }

  private LanguageDriver getLanguageDriver(Method method) {
    Lang lang = method.getAnnotation(Lang.class);
    Class<? extends LanguageDriver> langClass = null;
    if (lang != null) {
      langClass = lang.value();
    }
    return configuration.getLanguageDriver(langClass);
  }

  private Class<?> getParameterType(Method method) {
    Class<?> parameterType = null;
    Class<?>[] parameterTypes = method.getParameterTypes();
    for (Class<?> currentParameterType : parameterTypes) {
      if (!RowBounds.class.isAssignableFrom(currentParameterType) && !ResultHandler.class.isAssignableFrom(currentParameterType)) {
        if (parameterType == null) {
          parameterType = currentParameterType;
        } else {
          // issue #135
          parameterType = ParamMap.class;
        }
      }
    }
    return parameterType;
  }

  private Class<?> getReturnType(Method method) {
    Class<?> returnType = method.getReturnType();
    Type resolvedReturnType = TypeParameterResolver.resolveReturnType(method, type);
    if (resolvedReturnType instanceof Class) {
      returnType = (Class<?>) resolvedReturnType;
      if (returnType.isArray()) {
        returnType = returnType.getComponentType();
      }
      // gcode issue #508
      if (void.class.equals(returnType)) {
        ResultType rt = method.getAnnotation(ResultType.class);
        if (rt != null) {
          returnType = rt.value();
        }
      }
    } else if (resolvedReturnType instanceof ParameterizedType) {
      ParameterizedType parameterizedType = (ParameterizedType) resolvedReturnType;
      Class<?> rawType = (Class<?>) parameterizedType.getRawType();
      if (Collection.class.isAssignableFrom(rawType) || Cursor.class.isAssignableFrom(rawType)) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (actualTypeArguments != null && actualTypeArguments.length == 1) {
          Type returnTypeParameter = actualTypeArguments[0];
          if (returnTypeParameter instanceof Class<?>) {
            returnType = (Class<?>) returnTypeParameter;
          } else if (returnTypeParameter instanceof ParameterizedType) {
            // (gcode issue #443) actual type can be a also a parameterized type
            returnType = (Class<?>) ((ParameterizedType) returnTypeParameter).getRawType();
          } else if (returnTypeParameter instanceof GenericArrayType) {
            Class<?> componentType = (Class<?>) ((GenericArrayType) returnTypeParameter).getGenericComponentType();
            // (gcode issue #525) support List<byte[]>
            returnType = Array.newInstance(componentType, 0).getClass();
          }
        }
      } else if (method.isAnnotationPresent(MapKey.class) && Map.class.isAssignableFrom(rawType)) {
        // (gcode issue 504) Do not look into Maps if there is not MapKey annotation
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        if (actualTypeArguments != null && actualTypeArguments.length == 2) {
          Type returnTypeParameter = actualTypeArguments[1];
          if (returnTypeParameter instanceof Class<?>) {
            returnType = (Class<?>) returnTypeParameter;
          } else if (returnTypeParameter instanceof ParameterizedType) {
            // (gcode issue 443) actual type can be a also a parameterized type
            returnType = (Class<?>) ((ParameterizedType) returnTypeParameter).getRawType();
          }
        }
      } else if (Optional.class.equals(rawType)) {
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        Type returnTypeParameter = actualTypeArguments[0];
        if (returnTypeParameter instanceof Class<?>) {
          returnType = (Class<?>) returnTypeParameter;
        }
      }
    }

    return returnType;
  }

  private void applyResults(Result[] results, Class<?> resultType, List<ResultMapping> resultMappings) {
    for (Result result : results) {
      List<ResultFlag> flags = new ArrayList<>();
      if (result.id()) {
        flags.add(ResultFlag.ID);
      }
      @SuppressWarnings("unchecked")
      Class<? extends TypeHandler<?>> typeHandler = (Class<? extends TypeHandler<?>>)
        ((result.typeHandler() == UnknownTypeHandler.class) ? null : result.typeHandler());
      boolean hasNestedResultMap = hasNestedResultMap(result);
      ResultMapping resultMapping = assistant.buildResultMapping(
        resultType,
        nullOrEmpty(result.property()),
        nullOrEmpty(result.column()),
        result.javaType() == void.class ? null : result.javaType(),
        result.jdbcType() == JdbcType.UNDEFINED ? null : result.jdbcType(),
        hasNestedSelect(result) ? nestedSelectId(result) : null,
        hasNestedResultMap ? nestedResultMapId(result) : null,
        null,
        hasNestedResultMap ? findColumnPrefix(result) : null,
        typeHandler,
        flags,
        null,
        null,
        isLazy(result));
      resultMappings.add(resultMapping);
    }
  }

  private String findColumnPrefix(Result result) {
    String columnPrefix = result.one().columnPrefix();
    if (columnPrefix.length() < 1) {
      columnPrefix = result.many().columnPrefix();
    }
    return columnPrefix;
  }

  private String nestedResultMapId(Result result) {
    String resultMapId = result.one().resultMap();
    if (resultMapId.length() < 1) {
      resultMapId = result.many().resultMap();
    }
    if (!resultMapId.contains(".")) {
      resultMapId = type.getName() + "." + resultMapId;
    }
    return resultMapId;
  }

  private boolean hasNestedResultMap(Result result) {
    if (result.one().resultMap().length() > 0 && result.many().resultMap().length() > 0) {
      throw new BuilderException("Cannot use both @One and @Many annotations in the same @Result");
    }
    return result.one().resultMap().length() > 0 || result.many().resultMap().length() > 0;
  }

  private String nestedSelectId(Result result) {
    String nestedSelect = result.one().select();
    if (nestedSelect.length() < 1) {
      nestedSelect = result.many().select();
    }
    if (!nestedSelect.contains(".")) {
      nestedSelect = type.getName() + "." + nestedSelect;
    }
    return nestedSelect;
  }

  private boolean isLazy(Result result) {
    boolean isLazy = configuration.isLazyLoadingEnabled();
    if (result.one().select().length() > 0 && FetchType.DEFAULT != result.one().fetchType()) {
      isLazy = result.one().fetchType() == FetchType.LAZY;
    } else if (result.many().select().length() > 0 && FetchType.DEFAULT != result.many().fetchType()) {
      isLazy = result.many().fetchType() == FetchType.LAZY;
    }
    return isLazy;
  }

  private boolean hasNestedSelect(Result result) {
    if (result.one().select().length() > 0 && result.many().select().length() > 0) {
      throw new BuilderException("Cannot use both @One and @Many annotations in the same @Result");
    }
    return result.one().select().length() > 0 || result.many().select().length() > 0;
  }

  private void applyConstructorArgs(Arg[] args, Class<?> resultType, List<ResultMapping> resultMappings) {
    for (Arg arg : args) {
      List<ResultFlag> flags = new ArrayList<>();
      flags.add(ResultFlag.CONSTRUCTOR);
      if (arg.id()) {
        flags.add(ResultFlag.ID);
      }
      @SuppressWarnings("unchecked")
      //@Arg 的类型转化期 为默认UnknownTypeHandler就将类型转化器置为null,为其他置为其他类型转化器
        Class<? extends TypeHandler<?>> typeHandler = (Class<? extends TypeHandler<?>>)
        (arg.typeHandler() == UnknownTypeHandler.class ? null : arg.typeHandler());
      //生成数据库与
      ResultMapping resultMapping = assistant.buildResultMapping(
        resultType,
        nullOrEmpty(arg.name()),
        nullOrEmpty(arg.column()),
        arg.javaType() == void.class ? null : arg.javaType(),
        arg.jdbcType() == JdbcType.UNDEFINED ? null : arg.jdbcType(),
        nullOrEmpty(arg.select()),
        nullOrEmpty(arg.resultMap()),
        null,
        nullOrEmpty(arg.columnPrefix()),
        typeHandler,
        flags,
        null,
        null,
        false);
      resultMappings.add(resultMapping);
    }
  }

  private String nullOrEmpty(String value) {
    return value == null || value.trim().length() == 0 ? null : value;
  }

  private KeyGenerator handleSelectKeyAnnotation(SelectKey selectKeyAnnotation, String baseStatementId, Class<?> parameterTypeClass, LanguageDriver languageDriver) {
    String id = baseStatementId + SelectKeyGenerator.SELECT_KEY_SUFFIX;
    Class<?> resultTypeClass = selectKeyAnnotation.resultType();
    StatementType statementType = selectKeyAnnotation.statementType();
    String keyProperty = selectKeyAnnotation.keyProperty();
    String keyColumn = selectKeyAnnotation.keyColumn();
    boolean executeBefore = selectKeyAnnotation.before();

    // defaults
    boolean useCache = false;
    KeyGenerator keyGenerator = NoKeyGenerator.INSTANCE;
    Integer fetchSize = null;
    Integer timeout = null;
    boolean flushCache = false;
    String parameterMap = null;
    String resultMap = null;
    ResultSetType resultSetTypeEnum = null;
    String databaseId = selectKeyAnnotation.databaseId().isEmpty() ? null : selectKeyAnnotation.databaseId();

    SqlSource sqlSource = buildSqlSource(selectKeyAnnotation, parameterTypeClass, languageDriver, null);
    SqlCommandType sqlCommandType = SqlCommandType.SELECT;

    assistant.addMappedStatement(id, sqlSource, statementType, sqlCommandType, fetchSize, timeout, parameterMap, parameterTypeClass, resultMap, resultTypeClass, resultSetTypeEnum,
      flushCache, useCache, false,
      keyGenerator, keyProperty, keyColumn, databaseId, languageDriver, null);

    id = assistant.applyCurrentNamespace(id, false);

    MappedStatement keyStatement = configuration.getMappedStatement(id, false);
    SelectKeyGenerator answer = new SelectKeyGenerator(keyStatement, executeBefore);
    configuration.addKeyGenerator(id, answer);
    return answer;
  }

  private SqlSource buildSqlSource(Annotation annotation, Class<?> parameterType, LanguageDriver languageDriver,
                                   Method method) {
    if (annotation instanceof Select) {
      return buildSqlSourceFromStrings(((Select) annotation).value(), parameterType, languageDriver);
    } else if (annotation instanceof Update) {
      return buildSqlSourceFromStrings(((Update) annotation).value(), parameterType, languageDriver);
    } else if (annotation instanceof Insert) {
      return buildSqlSourceFromStrings(((Insert) annotation).value(), parameterType, languageDriver);
    } else if (annotation instanceof Delete) {
      return buildSqlSourceFromStrings(((Delete) annotation).value(), parameterType, languageDriver);
    } else if (annotation instanceof SelectKey) {
      return buildSqlSourceFromStrings(((SelectKey) annotation).statement(), parameterType, languageDriver);
    }
    return new ProviderSqlSource(assistant.getConfiguration(), annotation, type, method);
  }

  private SqlSource buildSqlSourceFromStrings(String[] strings, Class<?> parameterTypeClass,
                                              LanguageDriver languageDriver) {
    return languageDriver.createSqlSource(configuration, String.join(" ", strings).trim(), parameterTypeClass);
  }

  @SafeVarargs
  private final Optional<AnnotationWrapper> getAnnotationWrapper(Method method, boolean errorIfNoMatch,
                                                                 Class<? extends Annotation>... targetTypes) {
    return getAnnotationWrapper(method, errorIfNoMatch, Arrays.asList(targetTypes));
  }

  private Optional<AnnotationWrapper> getAnnotationWrapper(Method method, boolean errorIfNoMatch,
                                                           Collection<Class<? extends Annotation>> targetTypes) {
    //从配置类获取数据库id  数据库id 来区分不同数据库如Mysql，Oracle等
    String databaseId = configuration.getDatabaseId();
    //分析方法上是targetTypes中那种注解
    //一种databaseId拥有2个以上上述注解报错  解释：一个方法不可能即使删除又是新增
    Map<String, AnnotationWrapper> statementAnnotations = targetTypes.stream()
      .flatMap(x -> Arrays.stream(method.getAnnotationsByType(x))).map(AnnotationWrapper::new)
      .collect(Collectors.toMap(AnnotationWrapper::getDatabaseId, x -> x, (existing, duplicate) -> {
        throw new BuilderException(String.format("Detected conflicting annotations '%s' and '%s' on '%s'.",
          existing.getAnnotation(), duplicate.getAnnotation(),
          method.getDeclaringClass().getName() + "." + method.getName()));
      }));
    //注解包装类
    AnnotationWrapper annotationWrapper = null;
    if (databaseId != null) {
      //数据库id不为空
      //注解包装对象赋值
      annotationWrapper = statementAnnotations.get(databaseId);
    }
    if (annotationWrapper == null) {
      /** 由AnnotationWrapper的databaseId初始化，在annotationWrapper为null的时候尝试获取不存在注解的databaseId进行初始化
       *  if (annotation instanceof Select) {
       *         databaseId = ((Select) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.SELECT;
       *       } else if (annotation instanceof Update) {
       *         databaseId = ((Update) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.UPDATE;
       *       } else if (annotation instanceof Insert) {
       *         databaseId = ((Insert) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.INSERT;
       *       } else if (annotation instanceof Delete) {
       *         databaseId = ((Delete) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.DELETE;
       *       } else if (annotation instanceof SelectProvider) {
       *         databaseId = ((SelectProvider) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.SELECT;
       *       } else if (annotation instanceof UpdateProvider) {
       *         databaseId = ((UpdateProvider) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.UPDATE;
       *       } else if (annotation instanceof InsertProvider) {
       *         databaseId = ((InsertProvider) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.INSERT;
       *       } else if (annotation instanceof DeleteProvider) {
       *         databaseId = ((DeleteProvider) annotation).databaseId();
       *         sqlCommandType = SqlCommandType.DELETE;
       *       } else {
       *         sqlCommandType = SqlCommandType.UNKNOWN;
       *         if (annotation instanceof Options) {
       *           databaseId = ((Options) annotation).databaseId();
       *         } else if (annotation instanceof SelectKey) {
       *           databaseId = ((SelectKey) annotation).databaseId();
       *         } else {
       *           databaseId = "";
       *         }
       *         }
       */
      annotationWrapper = statementAnnotations.get("");
    }
    //存在注释，但指定的databaseId没有匹配  即注解上的和执行的数据库没匹配上
    if (errorIfNoMatch && annotationWrapper == null && !statementAnnotations.isEmpty()) {
      // Annotations exist, but there is no matching one for the specified databaseId
      throw new BuilderException(
        String.format(
          "Could not find a statement annotation that correspond a current database or default statement on method '%s.%s'. Current database id is [%s].",
          method.getDeclaringClass().getName(), method.getName(), databaseId));
    }
    //返回注释的包装类 此时确定是那种包装 以便sql解析执行
    return Optional.ofNullable(annotationWrapper);
  }

  private class AnnotationWrapper {
    private final Annotation annotation;
    private final String databaseId;
    private final SqlCommandType sqlCommandType;

    AnnotationWrapper(Annotation annotation) {
      super();
      this.annotation = annotation;
      if (annotation instanceof Select) {
        databaseId = ((Select) annotation).databaseId();
        sqlCommandType = SqlCommandType.SELECT;
      } else if (annotation instanceof Update) {
        databaseId = ((Update) annotation).databaseId();
        sqlCommandType = SqlCommandType.UPDATE;
      } else if (annotation instanceof Insert) {
        databaseId = ((Insert) annotation).databaseId();
        sqlCommandType = SqlCommandType.INSERT;
      } else if (annotation instanceof Delete) {
        databaseId = ((Delete) annotation).databaseId();
        sqlCommandType = SqlCommandType.DELETE;
      } else if (annotation instanceof SelectProvider) {
        databaseId = ((SelectProvider) annotation).databaseId();
        sqlCommandType = SqlCommandType.SELECT;
      } else if (annotation instanceof UpdateProvider) {
        databaseId = ((UpdateProvider) annotation).databaseId();
        sqlCommandType = SqlCommandType.UPDATE;
      } else if (annotation instanceof InsertProvider) {
        databaseId = ((InsertProvider) annotation).databaseId();
        sqlCommandType = SqlCommandType.INSERT;
      } else if (annotation instanceof DeleteProvider) {
        databaseId = ((DeleteProvider) annotation).databaseId();
        sqlCommandType = SqlCommandType.DELETE;
      } else {
        sqlCommandType = SqlCommandType.UNKNOWN;
        if (annotation instanceof Options) {
          databaseId = ((Options) annotation).databaseId();
        } else if (annotation instanceof SelectKey) {
          databaseId = ((SelectKey) annotation).databaseId();
        } else {
          databaseId = "";
        }
      }
    }

    Annotation getAnnotation() {
      return annotation;
    }

    SqlCommandType getSqlCommandType() {
      return sqlCommandType;
    }

    String getDatabaseId() {
      return databaseId;
    }
  }
}
